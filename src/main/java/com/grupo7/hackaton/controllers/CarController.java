package com.grupo7.hackaton.controllers;

import com.grupo7.hackaton.models.CarModel;
import com.grupo7.hackaton.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/hackaton")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})

public class CarController {

    @Autowired
    CarService carService;

    @GetMapping("/cars")
    public ResponseEntity<List<CarModel>> getCars(){
        System.out.println("getCars");

        return new ResponseEntity<>(
                this.carService.findAll(),
                HttpStatus.OK
        ) ;
    }

    @PostMapping("/cars")
    public ResponseEntity<CarModel> addCar(@RequestBody CarModel car){
        System.out.println("addCar");
        System.out.println("La id del car a crear es " + car.getId());
        System.out.println("La descripción del car a crear es " + car.getDesc());
        System.out.println("El precio del car a crear es " + car.getPrice());

        return new ResponseEntity<>(
                this.carService.add(car),
                HttpStatus.CREATED
        );
    }


    @GetMapping("/cars/{id}")
    public  ResponseEntity<Object> getCarById(@PathVariable String id){
        System.out.println("getCarById");
        System.out.println("La id del car a buscar es " + id);

        Optional<CarModel> result = this.carService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Car no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/cars/{id}")
    public ResponseEntity<CarModel> updateCar(@RequestBody CarModel car,@PathVariable String id){
        System.out.println("updateCar");
        System.out.println("La id del car que se va a actualizar en parámetro es " + id);
        System.out.println("La id del car que se va a actualizar es " + car.getId());
        System.out.println("La descripción del car que se va a actualizar " + car.getDesc());
        System.out.println("El precio del producto que se va a actualizar " + car.getPrice());
        System.out.println("El color del car que se va a actualizar " + car.getColor());
        System.out.println("El tipo del car que se va a actualizar " + car.getFoto());

        return new ResponseEntity<>(this.carService.update(car), HttpStatus.OK);
    }

    @DeleteMapping("/cars/{id}")
    public ResponseEntity<String> deleteCar(@PathVariable String id){
        System.out.println("deleteCar");
        System.out.println("la id del car a borrar " + id);

        boolean deleteProduct = this.carService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Car borrado" : "Car no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


}
