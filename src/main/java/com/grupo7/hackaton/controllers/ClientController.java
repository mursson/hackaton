package com.grupo7.hackaton.controllers;

import com.grupo7.hackaton.models.CarModel;
import com.grupo7.hackaton.models.ClientModel;
import com.grupo7.hackaton.services.CarService;
import com.grupo7.hackaton.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/hackaton")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})


public class ClientController {
    @Autowired
    ClientService clientService;

    @GetMapping("/clients")
    public ResponseEntity<List<ClientModel>> getClients(){
        System.out.println("getClients");

        return new ResponseEntity<>(
                this.clientService.findAll(),
                HttpStatus.OK
        ) ;
    }

    @PostMapping("/clients")
    public ResponseEntity<ClientModel> addClient(@RequestBody ClientModel client){
        System.out.println("addClient");
        System.out.println("La id del client a crear es " + client.getId());
        System.out.println("El nombre del client a crear es " + client.getName());
        System.out.println("La edad del client a crear es " + client.getAge());

        return new ResponseEntity<>(
                this.clientService.add(client),
                HttpStatus.CREATED
        );
    }


    @GetMapping("/clients/{id}")
    public  ResponseEntity<Object> getClientById(@PathVariable String id){
        System.out.println("getClientById");
        System.out.println("La id del client a buscar es " + id);

        Optional<ClientModel> result = this.clientService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Client no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/clients/{id}")
    public ResponseEntity<ClientModel> updateClient(@RequestBody ClientModel client,@PathVariable String id){
        System.out.println("updateClient");
        System.out.println("La id del client que se va a actualizar en parámetro es " + id);
        System.out.println("La id del client que se va a actualizar es " + client.getId());
        System.out.println("El nombre del client que se va a actualizar " + client.getName());
        System.out.println("La edad del client que se va a actualizar " + client.getAge());

        return new ResponseEntity<>(this.clientService.update(client), HttpStatus.OK);
    }

    @DeleteMapping("/clients/{id}")
    public ResponseEntity<String> deleteClient(@PathVariable String id){
        System.out.println("deleteClient");
        System.out.println("la id del client a borrar " + id);

        boolean deleteProduct = this.clientService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Client borrado" : "Client no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


}
