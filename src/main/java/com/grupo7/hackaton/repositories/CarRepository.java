package com.grupo7.hackaton.repositories;


import com.grupo7.hackaton.HackatonApplication;
import com.grupo7.hackaton.models.CarModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public class CarRepository {
    public List<CarModel> findAll() {
        System.out.println(("findAll en ProductRepository"));

        return HackatonApplication.carModels;
    }

    public CarModel save (CarModel car){
        System.out.println("Save in CarRepository");

        HackatonApplication.carModels.add(car);

        return car;
    }



    public Optional<CarModel> findById(String id){
        System.out.println("findById en CarRepository");

        Optional<CarModel> result = Optional.empty();

        for (CarModel carInList : HackatonApplication.carModels){
            if (carInList.getId().equals(id)){
                System.out.println("Car encontrado");
                result = Optional.of(carInList);
            }
        }

        return result;
    }

    public CarModel update(CarModel car){
        System.out.println("Update en CarRepository");

        Optional<CarModel> carToUpdate = this.findById(car.getId());

        if (carToUpdate.isPresent() == true){
            System.out.println("Car para actualizar encontrado");

            CarModel carFromList = carToUpdate.get();

            carFromList.setDesc(car.getDesc());
            carFromList.setPrice(car.getPrice());
            carFromList.setColor(car.getColor());
            carFromList.setFoto(car.getFoto());

        }

        return car;
    }

    public void delete(CarModel car){
        System.out.println("delete en CarRepository");
        System.out.println("Borrando car");

        HackatonApplication.carModels.remove(car);

    }
}
