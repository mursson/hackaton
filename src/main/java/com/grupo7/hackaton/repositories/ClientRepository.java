package com.grupo7.hackaton.repositories;

import com.grupo7.hackaton.HackatonApplication;
import com.grupo7.hackaton.models.CarModel;
import com.grupo7.hackaton.models.ClientModel;
import org.springframework.stereotype.Repository;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ClientRepository {
    public List<ClientModel> findAll() {
        System.out.println(("findAll en ProductRepository"));

        return HackatonApplication.clientModels;
    }

    public ClientModel save(ClientModel user) {
        System.out.println("Save en ClientRepository");

        HackatonApplication.clientModels.add(user);

        return user;
    }

    public Optional<ClientModel> findById(String id){
        System.out.println("findById en ClientRepository " + id);

        Optional<ClientModel> result = Optional.empty();

        for (ClientModel  userInList : HackatonApplication.clientModels){
            if (userInList.getId().equals(id)){
                System.out.println("Client encontrado");
                result = Optional.of(userInList);
            }
        }

        return result;
    }

    public ClientModel update(ClientModel client){
        System.out.println("Update en UserRepository");

        Optional<ClientModel> clientToUpdate = this.findById(client.getId());

        if (clientToUpdate.isPresent() == true){
            System.out.println("Client para actualizar encontrado");

            ClientModel userFromList = clientToUpdate.get();

            userFromList.setName(client.getName());
            userFromList.setAge(client.getAge());
        }

        return client;
    }

    public void delete(ClientModel client){
        System.out.println("delete en ClientRepository");
        System.out.println("Borrando client");

        HackatonApplication.clientModels.remove(client);

    }

}
