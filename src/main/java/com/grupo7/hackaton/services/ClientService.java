package com.grupo7.hackaton.services;

import ch.qos.logback.core.net.server.Client;
import com.grupo7.hackaton.models.CarModel;
import com.grupo7.hackaton.models.ClientModel;
import com.grupo7.hackaton.repositories.CarRepository;
import com.grupo7.hackaton.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    @Autowired
    ClientRepository clientRepository;

    public List<ClientModel> findAll() {
        System.out.println("findAll en ClientService");

        return this.clientRepository.findAll();
    }

    public ClientModel add (ClientModel client) {
        System.out.println("add en ClientService");

        return this.clientRepository.save(client);
    }

    public Optional<ClientModel> findById(String id){
        System.out.println("findById en ClientService" + id);

        return  this.clientRepository.findById(id);
    }

    public ClientModel update(ClientModel clientModel) {
        System.out.println("update en ClientService");

        return this.clientRepository.update(clientModel);
    }

    public boolean delete(String id) {
        System.out.println("delete en ClientService");

        boolean result = false;

        Optional<ClientModel> clientToDelete = this.findById(id);
        if (clientToDelete.isPresent()){

            result = true;
            this.clientRepository.delete(clientToDelete.get());
        }
        return result;
    }

}
